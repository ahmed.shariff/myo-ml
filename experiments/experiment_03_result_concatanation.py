import numpy as np
import mlflow
import json
from pathlib import Path
from mlpipeline.base import ExperimentABC
from mlpipeline import Versions, MetricContainer

from parameters import get_data_parameters, PARTICIPANTS, PARTICIPANTS_OA, PARTICIPANTS_YA
from data_utils import log_conf_metrix


class Experiment(ExperimentABC):
    def evaluate_loop(self, input_fn, **kwargs):
        metricContainer = MetricContainer(["train_accuracy_avg", "test_accuracy_avg"])
        # , "train_accuracy_calced", "test_accuracy_calced"])
        train_conf_metrix = []
        test_conf_metrix = []
        encoding = None
        mlflow.log_param("classes", get_data_parameters(self.current_version.participants_list[0]).classes)
        for participant in self.current_version.participants_list:
            version_name = "{}_{}".format(
                self.current_version.version_name,
                participant)
            model_path = self.current_version.models_path / "{}-{}".format(
                self.current_version.experiment_name,
                version_name)
            train_conf_metrix.append(np.loadtxt(model_path / "conf_matrix_train.txt"))
            test_conf_metrix.append(np.loadtxt(model_path / "conf_matrix_test.txt"))
            for suffix in ['test', 'train']:
                with open(model_path / f"encoding_{suffix}.json") as f:
                    temp_encoding = json.load(f)
                    if encoding is not None and encoding != temp_encoding:
                        raise Exception(f'encoding missmatch, {encoding}, {temp_encoding}')
                    encoding = temp_encoding

            run = mlflow.search_runs([mlflow.tracking.MlflowClient(mlflow.get_tracking_uri())
                                      .get_experiment_by_name(
                                          str(self.current_version.experiment_name) + ".py").experiment_id],
                                     f"tags.mlflow.runName='{version_name}'").iloc[0]
            metricContainer.train_accuracy_avg.update(run["metrics.TRAIN_score_calced"])
            metricContainer.test_accuracy_avg.update(run["metrics.TEST_score_calced"])
        train_conf_metrix = np.sum(train_conf_metrix, axis=0)
        test_conf_metrix = np.sum(test_conf_metrix, axis=0)
        # metricContainer.train_accuracy_calced.update((train_conf_metrix.diagonal().sum()/train_conf_metrix.sum()).item())
        # metricContainer.test_accuracy_calced.update((test_conf_metrix.diagonal().sum()/test_conf_metrix.sum()).item())
        log_conf_metrix(train_conf_metrix, "train", self.experiment_dir, encoding)
        log_conf_metrix(test_conf_metrix, "test", self.experiment_dir, encoding)
        return metricContainer


v = Versions()


def add_version(name, version_name, experiment_name, data_type, use_pca, participants):
    participants_list = PARTICIPANTS if participants == "all" else PARTICIPANTS_OA \
        if participants == "oa" else PARTICIPANTS_YA
    v.add_version(name,
                  version_name=version_name,
                  experiment_name=experiment_name,
                  data_type=data_type,
                  use_pca=use_pca,
                  participants=participants,
                  participants_list=participants_list,
                  models_path=Path("outputs/experiment_ckpts/"))


# ###################### ALL ################################
add_version("ALL_CLASSES_with_all_all_participants", "ALL_CLASSES_with_all",
            "experiment_01_classification_models", "all", False, "all")
# add_version("ALL_CLASSES_with_imu_all_participants", "ALL_CLASSES_with_imu",
#             "experiment_01_classification_models", "imu", False, "all")
# add_version("ALL_CLASSES_with_emg_all_participants", "ALL_CLASSES_with_emg",
#             "experiment_01_classification_models", "emg", False, "all")

# add_version("ensemble_all_all_participants", "ensemble_all",
#             "experiment_02_ensemble_model", "all", False, "all")
# add_version("ensemble_imu_all_participants", "ensemble_imu",
#             "experiment_02_ensemble_model", "imu", False, "all")
# add_version("ensemble_emg_all_participants", "ensemble_emg",
#             "experiment_02_ensemble_model", "emg", False, "all")
add_version("ensemble_joined_all_participants", "ensemble_joined",
            "experiment_02_ensemble_model", "all", False, "all")

# ###################### OA ################################
add_version("ALL_CLASSES_with_all_oa_participants", "ALL_CLASSES_with_all",
            "experiment_01_classification_models", "all", False, "oa")
# add_version("ALL_CLASSES_with_imu_oa_participants", "ALL_CLASSES_with_imu",
#             "experiment_01_classification_models", "imu", False, "oa")
# add_version("ALL_CLASSES_with_emg_oa_participants", "ALL_CLASSES_with_emg",
#             "experiment_01_classification_models", "emg", False, "oa")

# add_version("ensemble_all_oa_participants", "ensemble_all",
#             "experiment_02_ensemble_model", "all", False, "oa")
# add_version("ensemble_imu_oa_participants", "ensemble_imu",
#             "experiment_02_ensemble_model", "imu", False, "oa")
# add_version("ensemble_emg_oa_participants", "ensemble_emg",
#             "experiment_02_ensemble_model", "emg", False, "oa")
add_version("ensemble_joined_oa_participants", "ensemble_joined",
            "experiment_02_ensemble_model", "all", False, "oa")

# ###################### YA ################################
add_version("ALL_CLASSES_with_all_ya_participants", "ALL_CLASSES_with_all",
            "experiment_01_classification_models", "all", False, "ya")
# add_version("ALL_CLASSES_with_imu_ya_participants", "ALL_CLASSES_with_imu",
#             "experiment_01_classification_models", "imu", False, "ya")
# add_version("ALL_CLASSES_with_emg_ya_participants", "ALL_CLASSES_with_emg",
#             "experiment_01_classification_models", "emg", False, "ya")

# add_version("ensemble_all_ya_participants", "ensemble_all",
#             "experiment_02_ensemble_model", "all", False, "ya")
# add_version("ensemble_imu_ya_participants", "ensemble_imu",
#             "experiment_02_ensemble_model", "imu", False, "ya")
# add_version("ensemble_emg_ya_participants", "ensemble_emg",
#             "experiment_02_ensemble_model", "emg", False, "ya")
add_version("ensemble_joined_ya_participants", "ensemble_joined",
            "experiment_02_ensemble_model", "all", False, "ya")

# v.filter_versions(whitelist_versions=['ensemble_joined_ya_participants'])
EXPERIMENT = Experiment(v, False)
