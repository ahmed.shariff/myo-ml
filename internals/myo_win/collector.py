from ._device_listener import DeviceListener
from ._ffi import *


class MyoRaw(DeviceListener):
  """
  Collects EMG data in a queue with *n* maximum number of elements.
  """

  def __init__(self):
    self.emg_handlers  = []
    self.imu_handlers  = []
    self.arm_handlers  = []
    self.pose_handlers = []
    self.hub = None

  def connect(self):
      init()
      self.hub = Hub()


  def run(self):
      self.hub.run(self.on_event, 100)

  def disconnect(self):
      pass


  def add_emg_handler(self, h):
        self.emg_handlers.append(h)

  def add_imu_handler(self, h):
      self.imu_handlers.append(h)

  def add_pose_handler(self, h):
      self.pose_handlers.append(h)

  def add_arm_handler(self, h):
      self.arm_handlers.append(h)

  def on_connected(self, event):
    event.device.stream_emg(True)

  def on_emg(self, event):
      for h in self.emg_handlers:
          h(event.emg)

  def on_imu(self, quat):
      for h in self.imu_handlers:
          h(quat)

  def on_pose(self, p):
      for h in self.pose_handlers:
          h(p)

  def on_arm(self, arm, xdir):
      for h in self.arm_handlers:
          h(arm, xdir)
